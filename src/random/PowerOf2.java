package random;

public class PowerOf2 {

    public static boolean isPowerOf2(int n) {
        if (n <= 0) return false;
        int mask = 1;
        while (Math.abs(n) > 0) {
            if ((n & mask) == 1 && n != 1) {
                return false;
            }
            n >>= 1;
        }

        return true;
    }

    public static void main(String[] args) {
        System.out.println(isPowerOf2(16));
        System.out.println(isPowerOf2(-16));
        System.out.println(isPowerOf2(Integer.MAX_VALUE));
        System.out.println(isPowerOf2(Integer.MIN_VALUE));
    }

}
