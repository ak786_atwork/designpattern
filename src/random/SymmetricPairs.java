package random;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SymmetricPairs {

    public static void main(String[] args) {
        int[][] input = {{11, 20}, {30, 40}, {5, 10}, {40, 30}, {10, 5}};

        System.out.println(findPairs(input).toString());
    }

    public static List<List<Integer>> findPairs(int[][] input) {
        List<List<Integer>> result = new ArrayList<>();
        Set<String> set = new HashSet<>();
        String key = "";
        String value = "";
        String symmetricPair = "";

        for (int i = 0; i < input.length; i++) {
            symmetricPair = input[i][1] + ":" + input[i][0];
            if (set.contains(symmetricPair)) {
                List<Integer> list = new ArrayList<>();
                list.add(input[i][1]);
                list.add(input[i][0]);
                result.add(list);
            } else {
                set.add(input[i][0] + ":" + input[i][1]);
            }
        }

        return result;
    }

}
