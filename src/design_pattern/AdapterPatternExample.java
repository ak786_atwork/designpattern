package design_pattern;

interface WebDriver {
    public void getElement();

    public void selectElement();
}


class ChromeDriver implements WebDriver {

    @Override
    public void getElement() {
        System.out.println("get element from chrome");
    }

    @Override
    public void selectElement() {
        System.out.println("select element from chrome");
    }
}

//Adaptee
class IEDriver {
    public void findElement() {
        System.out.println("find element from ie");
    }

    public void clickElement() {
        System.out.println("click element from ie");
    }
}

class WebDriverAdapter implements WebDriver {

    IEDriver ieDriver;

    public WebDriverAdapter(IEDriver ieDriver) {
        this.ieDriver = ieDriver;
    }

    @Override
    public void getElement() {
        ieDriver.findElement();
    }

    @Override
    public void selectElement() {
        ieDriver.clickElement();
    }
}


public class AdapterPatternExample {

    public static void main(String[] args) {
        ChromeDriver c = new ChromeDriver();
        c.getElement();
        c.selectElement();

        IEDriver ieDriver = new IEDriver();
        ieDriver.clickElement();
        ieDriver.findElement();

        WebDriverAdapter webDriverAdapter = new WebDriverAdapter(ieDriver);
        webDriverAdapter.getElement();
        webDriverAdapter.selectElement();
    }

}
